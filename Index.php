<?php
    require_once('Animal.php');
    require_once('Ape.php');
    require_once('Frog.php');

    $sheep = new Animal('shaun');
    echo "Name : " . $sheep->name;
    echo "<br>";
    echo "Legs : " . $sheep->legs;
    echo "<br>";
    echo "Cold blooded : " . $sheep->cold_blooded;
    
    $kodok = new Frog('Buduk');
    echo "<br><br>Name : " . $kodok->name;
    echo "<br>";
    echo "Legs : " . $kodok->legs;
    echo "<br>";
    echo "Cold blooded : " . $kodok->cold_blooded;
    echo "<br>Jump : " . $kodok->jump();
    
    $sungokong = new Ape('kera sakti');
    echo "<br><br>Name : " . $sungokong->name;
    echo "<br>";
    echo "Legs : " . $sungokong->legs;
    echo "<br>";
    echo "Cold blooded : " . $sungokong->cold_blooded;
    echo "<br>Yell : " . $sungokong->yell();

?>